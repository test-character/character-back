FROM node:12.18-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 3030

RUN npm install --unsafe-perm=true --allow-root

# Se setea el Dockerfile para que arranque la app automáticamente
CMD ["npm", "run", "start"]