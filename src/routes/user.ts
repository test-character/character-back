import { Router } from 'express';
import userController from '../controllers/user';
import { validationMiddleware } from "../middleware";
import { UserDto } from "../models/validator";

const router = Router();

router.post('/register', validationMiddleware(UserDto), userController);

export default router;