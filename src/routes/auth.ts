import { Router } from 'express';
import authController from '../controllers/auth';
import { validationMiddleware } from "../middleware";
import { AuthDto } from "../models/validator";

const router = Router();

router.post('/login', validationMiddleware(AuthDto), authController.login);
router.post('/logout', authController.logout);

export default router;