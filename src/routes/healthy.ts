import { Router } from 'express';
import healthyController from '../controllers/healthy';

const router = Router();

router.get('/', healthyController);

export default router;