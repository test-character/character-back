import { Router } from 'express';
import characterController from '../controllers/character';
import { validateJwt } from "../middleware";

const router = Router();

router.get('/', [validateJwt], characterController);

export default router;