import { Router } from 'express';
import auth from './auth';
import user from './user';
import character from './character';
import healthy from './healthy';

const router = Router();

router.use('/auth', auth)
router.use('/user', user)
router.use('/character', character)
router.use('/healthy', healthy)

export default router;