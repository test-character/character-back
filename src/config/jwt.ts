import dotenv from "dotenv";
dotenv.config();

export default {
    expireTokenSecret: process.env.EXPIRE_TOKEN_SECRET || 120,
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET || "secret",
    accessTokenKey: process.env.ACCESS_JWT_KEY || "key",
};
