import dotenv from 'dotenv';
dotenv.config();

export default {
    port: Number(process.env.PORT) || 3001,
    hostname: process.env.HOSTNAME || 'localhost'
}