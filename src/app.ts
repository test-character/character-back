import express, { Application } from "express";
import helmet from "helmet";
import { setup, serve } from "swagger-ui-express";
import { load } from "yamljs";
import * as path from "path";

import { errorHandler, notFoundHandler, corsConfig } from "./middleware";

import { Redis } from "./redis";
import routes from "./routes";

import config from "./config/config";

const swaggerDocument = load(path.resolve(__dirname,'../swagger.yaml'));

export class App {
    private app: Application;

    constructor() {
        this.app = express();
        this.initClient();
        this.setting();
        this.route();
        this.middleware();
    }

    private initClient(): void {
        // eslint-disable-next-line no-new
        new Redis();
    }

    private setting(): void {
        this.app.use(helmet());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(corsConfig);
        this.app.options('*', corsConfig);
    }

    private middleware(): void {
        this.app.use(notFoundHandler);
        this.app.use(errorHandler);
    }

    private route(): void {
        this.app.use("/docs", serve, setup(swaggerDocument));
        this.app.use("/", routes);
    }

    async listen(): Promise<void> {
        this.app.listen({ ...config }, () =>
            console.log(`Server is listening ${config.hostname} ${config.port}`)
        );
    }
}
