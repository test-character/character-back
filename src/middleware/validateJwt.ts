import { Request, Response, NextFunction } from "express";
import { verify, sign } from "jsonwebtoken";
import { UnauthorizedException } from "../exceptions";
import { JwtPayload } from "../models/classes";
import configJwt from "../config/jwt";

export const validateJwt = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = <string>req.headers["authorization"];
    let jwtPayload;

    try {
        const tokenReplace: string = token.replace("Bearer ", "");
        jwtPayload = <any>verify(tokenReplace, configJwt.accessTokenSecret);
        res.locals.jwtPayload = jwtPayload;
        jwtPayload = new JwtPayload(
            jwtPayload.userId,
            jwtPayload.username,
            configJwt.accessTokenKey
        );
        const newToken = sign({ ...jwtPayload }, configJwt.accessTokenSecret, {
            expiresIn: configJwt.expireTokenSecret,
        });
        res.setHeader("token", newToken);

        next();
    } catch (error) {
        next(new UnauthorizedException());
    }
};
