import cors from "cors";

const corsOptions: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
    'Access-Control-Allow-Origin',
    'Access-Control-Allow-Headers',
    'Authorization'
  ],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,POST',
  preflightContinue: false,
};

export const corsConfig = cors(corsOptions)