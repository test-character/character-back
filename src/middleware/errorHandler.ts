import { Request, Response, NextFunction } from "express";
import { INTERNAL_SERVER_ERROR, getStatusText } from "http-status-codes";
import { HttpException } from "../exceptions";
import { ResponseHandlerService } from "../services/ResponseHandlerService";
import { DataResponse, Status } from "../models/classes";

export const errorHandler = (
    error: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const statusCode = error.status || INTERNAL_SERVER_ERROR;
    const message = error.message || getStatusText(INTERNAL_SERVER_ERROR);
    const responseHandlerService = new ResponseHandlerService();

    const status: Status = new Status(statusCode, message, true);
    const dataResponse: DataResponse = {
        status,
        data: []
    };
    console.log(error);
    console.log("Exception");
    responseHandlerService.responseHandler(res, dataResponse);
};
