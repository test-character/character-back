import { Request, Response, NextFunction } from "express";

export const notFoundHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

    const status = 404;
    const error = new Error('Route not found.');

    res.status(status).send({
        status: status,
        message: error.message
    });
};