import IORedis from 'ioredis';
import configRedis from './config/redis';
import { IStorage } from './interfaces/IStorage';

export class Redis implements IStorage {
    public clientRedis: any;
    
    constructor() {
        this.clientRedis = this.connect();
    }

    public connect(): any {
        console.log('Connect to Redis...')
        return new IORedis({
            ...configRedis
        })
    }

    public setData(key: string, value: string): Promise<any> {
        return this.clientRedis.set(key, value);
    }

    public getData(key: string): Promise<any> {
        return this.clientRedis.get(key);
    }

    public disconnect(): void {
        this.clientRedis.disconnect();
    }
}