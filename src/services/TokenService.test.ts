import { TokenService } from "./TokenService";
import { User } from "../models/classes/User";

describe("TokenService", () => {
    it("createToken", async () => {
        const tokenService = new TokenService();
        const user: User = {
            userId: "123456",
            name: "Pepito",
            lastName: "Perez",
            username: "pepito12",
            password: "pepito12",
            confirmPassword: "pepito12"
        };
        expect(tokenService.createToken(user)).not.toBeNull();
    });
});
