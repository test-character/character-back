import { Redis } from "../redis";
import { User, UserRegisterResponse } from "../models/classes";
import { RandomValue } from "../utils/RandomValue";
import { Validation } from "../utils/Validation";
import { UserExistsException, ConfirmPasswordException } from "../exceptions";

export class UserService {
    private redisClient: Redis;
    private randomValue: RandomValue;
    private validation: Validation;

    constructor() {
        this.redisClient = new Redis();
        this.randomValue = new RandomValue();
        this.validation = new Validation();
    }

    async register(user: User): Promise<UserRegisterResponse> {
        let register: boolean = false;
        let exception: any = {};
        if (user.password === user.confirmPassword) {
            const userIdGenerate: string = this.randomValue.randomString();
            const getUser = await this.redisClient.getData(user.username);
            if (!getUser) {
                const hashedPassword = this.validation.hashValue(
                    user.password,
                    10
                );
                user.password = hashedPassword;
                user.userId = userIdGenerate;
                const userParse = JSON.stringify(user);
                await this.redisClient.setData(user.username, userParse);
                register = true;
                user.password = "";
            } else {
                const dataUser = JSON.parse(getUser);
                exception = new UserExistsException(dataUser.username) 
            }
        } else {
            exception = new ConfirmPasswordException();
        }
        const userRegisterResponse: UserRegisterResponse = {
            user,
            register,
            exception
        };
        this.redisClient.disconnect();
        return userRegisterResponse;
    }
}
