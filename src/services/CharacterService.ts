import { CharacterResponse, Headers } from "../models/classes";
import { ApiClientService } from "./ApiClientService";
import configRickAndMortyApi from "../config/character";

export class CharacterService extends ApiClientService {
    
    async getCharacters(): Promise<CharacterResponse[]> {
        const characterResponses: CharacterResponse[] = [];
        const getCharacters: any = await this.get(
            configRickAndMortyApi.url,
            Headers
        );
        if (getCharacters && getCharacters.results) {
            getCharacters.results.forEach((elem) => {
                const characterResponse: CharacterResponse = {
                    name: elem.name,
                    status: elem.status,
                    species: elem.species,
                    gender: elem.gender,
                    image: elem.image,
                };
                characterResponses.push(characterResponse);
            });
        }
        return characterResponses;
    }
}
