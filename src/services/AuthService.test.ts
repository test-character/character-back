import { AuthService } from "./AuthService";
import { AuthLoginResponse, Auth, User } from "../models/classes";
import { Redis } from "../redis";
jest.mock("../redis");

describe("UserService", () => {
    it("register", async () => {
        const auth: Auth = {
            username: "pepito12",
            password: "pepito12"
        };
        const user: User = {
            userId: "123456",
            name: "Pepito",
            lastName: "Perez",
            username: "pepito12",
            password: "pepito12",
            confirmPassword: "pepito12"
        };
        const authLoginResponse: AuthLoginResponse = {
            user,
            auth: true,
            token: "123444566"
        };
        const authService = new AuthService();
        authService.login = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve(authLoginResponse);
                })
        );
        const redisClient = new Redis();
        redisClient.connect = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.getData = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.setData = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.disconnect = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.connect();
        redisClient.getData(user.username);
        redisClient.disconnect();
        authService.login(auth);
        expect(authService.login).toBeCalled();
    });
});
