import { Redis } from "../redis";
import { Auth, AuthLoginResponse, User } from "../models/classes";
import { TokenService } from "./TokenService";
import { Validation } from "../utils/Validation";

export class AuthService {
    private redisClient: Redis;
    private tokenService: TokenService;
    private validation: Validation;

    constructor() {
        this.redisClient = new Redis();
        this.tokenService = new TokenService();
        this.validation = new Validation();
    }
    
	async login(auth: Auth): Promise<AuthLoginResponse> {
        let authValidator: boolean = false;
        let user: User = new User();
        let token: string = "";
		const getUser = await this.redisClient.getData(auth.username);
		if (getUser) {
			const getUserParse: User = JSON.parse(getUser) as User;
			const passwordMatching = this.validation.checkValueIsValid(
				auth.password,
                getUserParse.password
			);
            if (passwordMatching) {
                user = getUserParse;
                user.password = "";
                authValidator = true;
                token = await this.tokenService.createToken(user);
            }
        }
        const authLoginResponse: AuthLoginResponse = {
            user,
            auth: authValidator,
            token
        };
        this.redisClient.disconnect();
        return authLoginResponse;
    }
}
