import { sign } from "jsonwebtoken";
import { User, JwtPayload } from "../models/classes";
import configJwt from "../config/jwt";

export class TokenService {
    async createToken(user: User): Promise<string> {
        const jwtPayload = new JwtPayload(
            user.userId || "",
            user.username,
            configJwt.accessTokenKey
        );
        return sign({ ...jwtPayload }, configJwt.accessTokenSecret, {
            expiresIn: configJwt.expireTokenSecret,
        });
    }
}
