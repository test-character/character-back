import { UserService } from "./UserService";
import { UserRegisterResponse, User } from "../models/classes";
import { Redis } from "../redis";
jest.mock("../redis");

describe("UserService", () => {
    it("register", async () => {
        const user: User = {
            userId: "123456",
            name: "Pepito",
            lastName: "Perez",
            username: "pepito12",
            password: "pepito12",
            confirmPassword: "pepito12"
        };
        const userRegisterResponse: UserRegisterResponse = {
            user,
            register: true,
            exception: {}
        };
        const userParse = JSON.stringify(user);

        const userService = new UserService();
        userService.register = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve(userRegisterResponse);
                })
        );
        const redisClient = new Redis();
        redisClient.connect = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.getData = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.setData = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.disconnect = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve({});
                })
        );
        redisClient.connect();
        redisClient.getData(user.username);
        redisClient.setData(user.username, userParse);
        redisClient.disconnect();
        userService.register(user);
        expect(userService.register).toBeCalled();
    });
});
