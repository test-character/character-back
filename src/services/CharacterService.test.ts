import { CharacterService } from "./CharacterService";
import { CharacterResponse } from "../models/classes";

describe("CharacterService", () => {
    it("getCharacters", async () => {
        const characterResponses: CharacterResponse[] = [];
        const characterService = new CharacterService();
        characterService.getCharacters = jest.fn(
            () =>
                new Promise((resolve, reject) => {
                    resolve(characterResponses);
                })
        );
        characterService.getCharacters();
        expect(characterService.getCharacters).toBeCalled();
    });
});
