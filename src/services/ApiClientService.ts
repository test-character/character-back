import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

export class ApiClientService {
    private api: any;

    public constructor(config?: AxiosRequestConfig) {
        this.api = axios.create(config);

        this.get = this.get.bind(this);
        this.post = this.post.bind(this);
    }

    public get<T, R = AxiosResponse<T>>(
        url: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        const axiosResponse = this.api
            .get(url, config)
            .then(this.success)
            .catch(this.error);
        return axiosResponse;
    }

    public post<T, B, R = AxiosResponse<T>>(
        url: string,
        data?: B,
        config?: AxiosRequestConfig
    ): Promise<R> {
        const axiosResponse = this.api
            .post(url, data, config)
            .then(this.success)
            .catch(this.error);
        return axiosResponse;
    }

    public success<T>(response: AxiosResponse<T>): T {
        return response.data;
    }

    public error(error: AxiosError<Error>) {
        throw error;
    }
}
