import { Response } from "express";
import { DataResponse } from "../models/classes/DataResponse";

export class ResponseHandlerService {
    async responseHandler(response: Response, dataResponse: DataResponse) {
        return response.status(dataResponse.status.code).json(dataResponse);
    }
}
