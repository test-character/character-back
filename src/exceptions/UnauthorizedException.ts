import { UNAUTHORIZED, getStatusText } from "http-status-codes";
import { HttpException } from "./HttpException";

export class UnauthorizedException extends HttpException {
    constructor() {
        super(UNAUTHORIZED, getStatusText(UNAUTHORIZED));
    }
}