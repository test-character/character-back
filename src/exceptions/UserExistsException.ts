import { BAD_REQUEST } from "http-status-codes";
import { HttpException } from "./HttpException";

export class UserExistsException extends HttpException {
  constructor(user: string) {
    super(BAD_REQUEST, `Username ${user} already exists`);
    }
}