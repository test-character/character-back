import { BAD_REQUEST } from "http-status-codes";
import { HttpException } from "./HttpException";

export class ConfirmPasswordException extends HttpException {
    constructor() {
        super(BAD_REQUEST, "Passwords do not match");
    }
}
