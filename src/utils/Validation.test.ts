import { Validation } from "./Validation";

describe("Validation", () => {
    const validation = new Validation();
    const text: string = "testtest";
    it("hashValue", async () => {
        expect(validation.hashValue(text)).not.toBeNull();
    });

    it("checkValueIsValid", async () => {
        const hashText: string = "123456789";
        expect(validation.checkValueIsValid(text, hashText)).toBeFalsy();
    });
});
