import { hashSync, compareSync } from "bcrypt";

export class Validation {

  hashValue(value: string, saltOrRounds?: number): string {
    return hashSync(value, saltOrRounds || 10);
  }

  checkValueIsValid(valueNotEncrypted: string, valueEncrypted : string): boolean {
    return compareSync(valueNotEncrypted, valueEncrypted);
  }

}