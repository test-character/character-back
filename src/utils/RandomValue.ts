const OPTIONS_STRING: string = "abcdefghijklmnopqrstuvwxyz0123456789";

export class RandomValue {
	
    public randomString(): string {
        let outString: string = "";

        for (let i = 0; i < 32; i++) {
            outString += OPTIONS_STRING.charAt(
                Math.floor(Math.random() * OPTIONS_STRING.length)
            );
        }

        return outString;
    }
}
