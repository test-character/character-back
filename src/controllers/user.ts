import { Request, Response, NextFunction } from "express";
import {
    CREATED,
    INTERNAL_SERVER_ERROR,
    getStatusText
} from "http-status-codes";
import {
    HttpException
} from "../exceptions";
import { UserService, ResponseHandlerService } from "../services";
import {
    User,
    DataResponse,
    Status,
    UserRegisterResponse
} from "../models/classes";

const register = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const responseHandlerService = new ResponseHandlerService();
        const userService = new UserService();
        const userCasting = req.body as User;
        const userRegisterResponse: UserRegisterResponse = await userService.register(
            userCasting
        );
        console.log(userRegisterResponse);
        console.log("..............userRegisterResponse");
        if (userRegisterResponse.register) {
            const status: Status = new Status(CREATED, getStatusText(CREATED));
            const dataResponse: DataResponse = {
                status,
                data: userRegisterResponse
            };
            responseHandlerService.responseHandler(res, dataResponse);
        } else {
            next(userRegisterResponse.exception);
        }
    } catch (error) {
        next(
            new HttpException(
                INTERNAL_SERVER_ERROR,
                getStatusText(INTERNAL_SERVER_ERROR),
                error
            )
        );
    }
};

export default register;
