import { Request, Response, NextFunction } from "express";
import { OK, getStatusText } from "http-status-codes";
import { DataResponse, Status, HealthyResponse } from "../models/classes";
import { ResponseHandlerService } from "../services/ResponseHandlerService";
import version from "../config/version";

const healthy = (req: Request, res: Response, next: NextFunction) => {
    const responseHandlerService = new ResponseHandlerService();
    const status: Status = {
        code: OK,
        message: getStatusText(OK),
    };
    const healthyResponse: HealthyResponse = {
        name: version.name,
        version: version.version,
        description: version.description,
    };
    const dataResponse: DataResponse = {
        status,
        data: healthyResponse,
    };
    responseHandlerService.responseHandler(res, dataResponse);
};

export default healthy;
