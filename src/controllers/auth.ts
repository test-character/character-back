import { Request, Response, NextFunction } from "express";
import {
    OK,
    INTERNAL_SERVER_ERROR,
    getStatusText,
} from "http-status-codes";
import { HttpException, UnauthorizedException } from "../exceptions";
import { AuthService, ResponseHandlerService } from "../services";
import { Auth, DataResponse, Status, AuthLoginResponse } from "../models/classes";

const login = async (req: Request, res: Response, next: NextFunction) => {
    try {
    const responseHandlerService = new ResponseHandlerService();
    const authService = new AuthService();
    const authCasting = req.body as Auth;
    const authLoginResponse: AuthLoginResponse = await authService.login(
        authCasting
    );
    if (authLoginResponse.auth) {
        const status: Status = new Status(OK, getStatusText(OK));
        const dataResponse: DataResponse = {
            status,
            data: authLoginResponse,
        };
        responseHandlerService.responseHandler(res, dataResponse);
    } else {
        next(new UnauthorizedException());
    }
    } catch (error) {
        next(
            new HttpException(
                INTERNAL_SERVER_ERROR,
                getStatusText(INTERNAL_SERVER_ERROR),
                error
            )
        );
    }
};

const logout = async (req: Request, res: Response, next: NextFunction) => {
    try {
    res.status(200).json({ success: true, data: req.body });
} catch (e) {
    next(
        new HttpException(
            INTERNAL_SERVER_ERROR,
            getStatusText(INTERNAL_SERVER_ERROR),
            e
        )
    );
}
};

export default { login, logout };
