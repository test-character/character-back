import { Request, Response, NextFunction } from "express";
import { OK, INTERNAL_SERVER_ERROR, getStatusText } from "http-status-codes";
import { HttpException } from "../exceptions";
import { CharacterService, ResponseHandlerService } from "../services";
import { DataResponse, Status, CharacterResponse } from "../models/classes";

const getCharacter = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const responseHandlerService = new ResponseHandlerService();
        const characterService = new CharacterService();
        const characterResponse: CharacterResponse[] = await characterService.getCharacters();
        const status: Status = new Status(OK, getStatusText(OK));
        const dataResponse: DataResponse = {
            status,
            data: characterResponse,
        };
        responseHandlerService.responseHandler(res, dataResponse);
    } catch (error) {
        next(
            new HttpException(
                INTERNAL_SERVER_ERROR,
                getStatusText(INTERNAL_SERVER_ERROR),
                error
            )
        );
    }
};

export default getCharacter;
