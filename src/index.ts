import { App } from "./app";
const main = async (): Promise<void> => {
  const app = new App();
  console.log('initialized server...')
  await app.listen();
}

main();