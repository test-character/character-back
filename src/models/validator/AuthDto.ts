import { IsString, Length, IsDefined } from "class-validator";

export class AuthDto {
    @IsString()
    @Length(8, 8)
    @IsDefined()
    public username!: string;

    @IsString()
    @Length(8, 8)
    @IsDefined()
    public password!: string;
}
