import { IsString, Length, IsDefined } from "class-validator";

export class UserDto {
    @IsString()
    @Length(3, 50)
    @IsDefined()
    public name!: string;

    @IsString()
    @Length(3, 50)
    @IsDefined()
    public lastName!: string;

    @IsString()
    @Length(8, 8)
    @IsDefined()
    public username!: string;

    @IsString()
    @Length(8, 8)
    @IsDefined()
    public password!: string;

    @IsString()
    @Length(8, 8)
    @IsDefined()
    public confirmPassword!: string;
}
