export class Status {
    public hasError?: boolean;
    public code: number;
    public message: string;

    constructor(code: number, message: string, hasError?: boolean) {
        this.code = code;
        this.message = message;
        this.hasError = hasError || false;
    }
}
