import { Status } from "./Status";

export class DataResponse<T = {}> {
    public status: Status;
    public data?: T;

    constructor() {
        this.data = {} as T;
        this.status = new Status(0, "");
    }
}