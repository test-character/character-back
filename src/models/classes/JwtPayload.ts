export class JwtPayload {
    public userId: string;
    public username: string;
    public iss?: string;

    constructor(userId: string, username: string, iss?: string) {
        this.userId = userId;
        this.username = username;
        this.iss = iss || "";
    }
}
