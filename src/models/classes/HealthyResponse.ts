export class HealthyResponse {
    public name: string;
    public version: string;
    public description: string;

    constructor() {
        this.name = "";
        this.version = "";
        this.description = "";
    }
}
