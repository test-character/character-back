export class User {
  public userId?: string;
  public name: string;
  public lastName: string;
  public username: string;
  public password: string;
  public confirmPassword: string;

  constructor() {
    this.userId = "";
    this.name = "";
    this.lastName = "";
    this.username = "";
    this.password = "";
    this.confirmPassword = "";
  }
}