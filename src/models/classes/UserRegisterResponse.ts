import { User } from "./User";

export class UserRegisterResponse {
    public user: User;
    public register: boolean;
    public exception: any;

    constructor() {
        this.user = new User();
        this.register = false;
        this.exception = {};
    }
}
