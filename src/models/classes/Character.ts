export class Character {
    public name: string;
    public status: string;
    public species: string;
    public gender: string;
    public image: string;

    constructor() {
        this.name = "";
        this.status = "";
        this.species = "";
        this.gender = "";
        this.image = "";
    }
}
