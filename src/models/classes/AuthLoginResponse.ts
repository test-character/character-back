import { User } from "./User";

export class AuthLoginResponse {
    public user: User;
    public auth: boolean;
    public token?: string;

    constructor() {
        this.user = new User();
        this.auth = false;
        this.token = "";
    }
}
