export interface IStorage {
    setData(key: string, value: string): Promise<any>;
    getData: (key: string, name: string) => Promise<boolean>;
}