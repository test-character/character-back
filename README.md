# Api Character
Api allows to display the list of Rick and Morty characters

# Getting Started And Installing
After cloning the project, enter the project folder and execute:
```
docker build -t <IMAGENAME> .
```

```
docker run -p 3030:3030 -h 0.0.0.0 -t <IMAGENAME>
```

# Run Local
```
npm run start/dev
```

```
Running on http://localhost:3030/
```

# Api doc - Swagger
```
http://localhost:3030/docs
```

# Run Build
```
npm run build
```

# Coverage
```
npm run test
```

# Linter
```
npm run linter:check
```